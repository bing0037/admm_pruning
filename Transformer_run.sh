# 1 Pre-train model:
python 11_Transformer_pretrain_model.py --train --save_model transformer_model.pt --batch_size 800 --epochs 50 # batch_size: 20 for PC, 800 for server

# +1) prune the model using irregular pruning:
python 11_Transformer_pretrain_model.py --train --irregular_pruning --save_model transformer_pruned_model_irregular.pt --batch_size 800 --epochs 1 # batch_size: 20 for PC, 800 for server



# +2) test the model (Original ADMM model or pruned model):
# test the original the pre-trained model:
python 11_Transformer_pretrain_model.py --test --load_model transformer_model.pt
# test the irregularly pruned model:
python 11_Transformer_pretrain_model.py --test --load_model transformer_pruned_model_irregular.pt


# 2 ADMM training:
# 2-1) ADMM:
python 12_Transformer_pruning_test.py --admm_transformer --batch_size 800 --load_model transformer_model.pt --sparsity_type irregular --combine_progressive --epochs 20 --rho 0.001 --rho_num 4 --lr 0.001 --lr_decay 20 --config_file config_transformer
# 2-2) Retrain:
python 12_Transformer_pruning_test.py --masked_retrain --batch_size 800 --load_model transformer_model.pt --sparsity_type irregular --combine_progressive --epochs 20 --rho 0.001 --rho_num 1 --lr 0.001 --lr_decay 20 --config_file config_transformer
