Functions: ADMM-based pruning

# How to use: based on Lenet model & mnist dataset:
MNIST_run.sh

# How to use: based on Transformer model & WikiText2:
Transformer_run.sh